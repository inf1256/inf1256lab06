/**
 * @author Johnny Tsheke @ UQAM
 * 2017-03-04
 */
package lab06;

import java.util.*;
import java.lang.Math;
public class ValeursFutures {

	
	public static void main(String[] args) {
		final double CENT = 100.0;//pour convertir pourcenage en nombre reel
		final int PMAX = 5;//nombre maximum des périodes
		int periode = 0; //ériode initiale
		double valeurInitiale = 0.0;//initialisation
        boolean valInitOk = false;
        double taux = 0.0;
        boolean tauxOk = false;
        Scanner clavier = new Scanner(System.in).useLocale(Locale.US);
        while(!valInitOk){//boucle de saisie et validation valeur initiale
        	System.out.println("Veuillez entrer la valeur initiale svp");
        	if(clavier.hasNextDouble()){
        		valeurInitiale = clavier.nextDouble();
        		valInitOk = true;//pour arreter la boucle
        	}else{
        		System.out.println("Valeur invalide. Il faut saisir un nombre");
        		clavier.next();//on passe pour lire la prochaine entrée
        	}
        }
        //Ici on a saisie et valider la valer initiale
        
        while(!tauxOk){//boucle de saisie et validation taux interet
        	System.out.println("Veuillez entrer le taux d'intérêt en pourcentage svp");
        	if(clavier.hasNextDouble()){
        		taux = clavier.nextDouble();
        		taux = taux/CENT;
        		tauxOk = true;//pour arreter la boucle
        	}else{
        		System.out.println("Valeur invalide. Il faut saisir un nombre");
        		clavier.next();//on passe pour lire la prochaine entrée
        	}
        }
        
        //Ici le taux d'intérêt est saisi et validé
        
        System.out.format("Valeur Initiale = %.2f %n", valeurInitiale);
        System.out.format("Taux Interet = %.2f %n", taux);
        System.out.format("Nombre période max = %d %n", PMAX);
        
        while(periode<=PMAX){
        	double valeurFuture = valeurInitiale * Math.pow((1 + taux),periode);
        	System.out.format("Période = %d et Valeur Future = %.2f %n", periode,valeurFuture);
        	periode = periode + 1;
        }
        clavier.close();
	}

}
