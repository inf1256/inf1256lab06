/**
 * @author Johnny Tsheke @ UQAM
 * 2017-03-04 
 */
package lab06;

import java.util.*;

public class ImpotFederalCanada2015 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Quelques constantes
		final double BORNE_TRANCHE1 = 44701.0;
		final double BORNE_TRANCHE2 = 89401.0;
		final double BORNE_TRANCHE3 = 138586.0;
		final double TAUX_TRANCHE1 = 0.15; //15%
		final double TAUX_TRANCHE2 = 0.22;
		final double TAUX_TRANCHE3 = 0.26;
		final double TAUX_TRANCHE4 = 0.29;
		double impFed = 0.0; /* Initialisation impôt fédéral */;
		double trancheSup = 0.0;
		double tranche1 = 0.0;
		double tranche2 = 6705.0;
		double tranche3 = 16539.0;
		double tranche4 = 29327.0;
		boolean revenuOk=false;//variable pour controler la boucle
		double revenu=0;//initialisation
		Scanner clavier = new Scanner(System.in).useLocale(Locale.US);//system local use pour écrire des décimaux avec des .
		do{ //boucle pour saisir et valider le revenu
			System.out.println("Veuillez saisir votre revenu svp");
			if(clavier.hasNextDouble()){
				revenu = clavier.nextDouble();
				revenuOk = true;//pour arreter la boucle
			}else{
				System.out.print("Valeur incorrecte. Il faut saisir un nombre.");
				clavier.next();//on passe pour lire la prochaine entrée
			}
		}while(!revenuOk);
		//a ce niveau un revenu valide est saisi
		//Atention, quand il y a des if imbriqués, ils sont évalués das l'ordre de leur apparition
		if(revenu<0){
			System.out.println("Erreur: Le revenu ne peut pas être négatif");
		}else if(revenu<BORNE_TRANCHE1){
			impFed = tranche1 + (revenu * TAUX_TRANCHE1);
		}else if(revenu<BORNE_TRANCHE2){
			trancheSup = revenu - 44701.0;
			impFed = tranche2 + (trancheSup * TAUX_TRANCHE2);
		}else if(revenu<BORNE_TRANCHE3){
			trancheSup = revenu - 89401.0;
			impFed = tranche3 + (trancheSup * TAUX_TRANCHE3);
		}else{//tranche 4
			trancheSup = revenu - 138586.0;
			impFed = tranche4 + (trancheSup * TAUX_TRANCHE4);
		}
		//ici on fini les calculs
		if(revenu>0){
			System.out.format("Le revenu est égal à %.2f %n", revenu);
			System.out.format("L'impot federal est égal à %.2f %n", impFed);
		}
		clavier.close();
	}

}
